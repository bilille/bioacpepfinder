#! /usr/bin/env python

"""
Author: Isabelle GUIGON
Date: 2021-02-16
Developed on Python 3.8
"""

import os
import sys
import shutil
import argparse
import configparser

from datetime import datetime
from lib.download_proteins import *
from lib.digestion import *
from lib.makeblastdb import *
from lib.blastp_wrapper import *
from lib.QSAR import *
from lib.peptide_lists_comparison import *
from lib.read_Peaks_file import *


# Global variables
coeff_DPPIV_1_file = "data/DPPIV_Z_Vscores_table1.tsv"
coeff_DPPIV_2_file = "data/DPPIV_Z_Vscores_table2.tsv"
coeff_ACE_file = "data/ACE_table.tsv"


def main():

    begin_time = datetime.now()

    desc = "Pipeline for the detection of potentially bioactive peptides in proteins."

    command = argparse.ArgumentParser(prog = 'BioacPepFinder.py',
        description = desc,
        usage = '%(prog)s [options]')

    # general options
    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        help = 'Path to your output directory (default: a folder is created in your input file directory)')

    command.add_argument('--force',
        action = 'store_true',
        help = 'If set, erase the content of --outdir if it already exists (WARNING: be sure of what you are doing!)')

    command.add_argument('-c', '--cpu', nargs = '?',
        type = int,
        default = 1,
        help = 'Number of CPUs to allocate to rpg and blast (default 1)')

    # input: either peptides (from Peaks)(no digestion needed) or proteic sequences or ids
    command.add_argument('-i', '--input', nargs = '+',
        type = str,
        required = True,
        help = 'Input file: proteic sequences (fasta) | list of genes/transcripts/proteic identifiers (txt) | Peaks file (required)')

    command.add_argument('--input-type', nargs = '?',
        type = str,
        required = True,
        help = "Type of input file: {'proteins', 'identifiers', 'Peaks'} (required)")

    command.add_argument('--id-type', nargs = '+',
        type = str,
        default = ['uniprot'],
        help = "Type of identifier (same number and order than --input-type entries): {'uniprot', 'ensembl', 'ncbi'} (default: uniprot)")

    # digestion options
    command.add_argument('--mode', nargs = '?',
        type = str,
        default = 's',
        help = 'Digestion mode. Either ‘s’, ‘sequential’, ‘c’ or ‘concurrent’ (default: s)')

    command.add_argument('-e', '--enzymes', nargs = '+',
        type = int,
        default = [42],
        help = 'Id of enzyme(s) to use (i.e. -e 0 5 10 to use enzymes 0, 5 and 10). Use rpg -l first to get enzyme ids (default 42: trypsin)')

    command.add_argument('-m', '--misscleavage', nargs = '?',
        type = int,
        default = 1,
        help = 'Number of miss-cleavages allowed ([0-3] default 1)')

    # blast options
    command.add_argument('-d', '--database', nargs = '?',
        type = str,
        help = 'Biopeptide database for usage with blastp (without extension)')

    command.add_argument('-f', '--fasta', nargs = '+',
        type = str,
        help = 'One or several input fasta file containing biopeptide sequences')

    command.add_argument('--evalue', nargs = '?',
        type = float,
        default = 0.1,
        help = 'Evalue threshold for blast (default 0.1)')

    command.add_argument('--id_threshold', nargs = '?',
        type = int,
        default = 100,
        help = 'Evalue threshold for percentage of identity (default 100)')

    command.add_argument('--pos_threshold', nargs = '?',
        type = int,
        default = 0,
        help = 'Evalue threshold for percentage of positives (default 0 - we do not filter on positives). --pos_threshold N implies --id_threshold N.')

    # comparison with a list of experimental peptides
    command.add_argument('--exp', nargs = '?',
        type = str,
        help = 'A list of experimental peptides (one peptide per line). If given, only results involving one of the given peptides will be printed.')

    args = command.parse_args()

    # check options
    if args.input_type not in ['proteins', 'identifiers', 'Peaks']:
        raise Exception("Input type must be 'proteins', 'identifiers' or 'Peaks'.")

    if args.mode not in ['s', 'sequential', 'c', 'concurrent']:
        raise Exception("Digestion mode must be either 's', 'sequential', 'c' or 'concurrent'.")

    # this is solely for more clarity in config file
    if args.mode == 's':
        args.mode = 'sequential'
    if args.mode == 'c':
        args.mode = 'concurrent'

    if not args.database and not args.fasta:
        error_message = "You must provide either a blastp database (without extension), "
        error_message += "or one or several fasta files of biopeptides with the activity in the header."
        raise Exception(error_message)

    # % identity threshold cannot be lower than % positives threshold
    if args.pos_threshold != 0:
        args.id_threshold = args.pos_threshold

    # Check if output directory exists. If --force we will overwrite it
    if os.path.exists(args.outdir):
        if args.force:
            try:
                print("Overwrite output directory {}".format(args.outdir))
                shutil.rmtree(args.outdir)
            except OSError as e:
                print("Error: %s : %s" % (args.outdir, e.strerror))
        else:
            raise Exception("Output directory already exists. Please give another directory or use option --force to overwrite it.")

    if not args.outdir:
        args.outdir = os.path.join( os.path.dirname(args.input[0]), 'bioacpepfinder_results_' + begin_time.strftime("%Y%m%d%H%M%S") )

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # if only one enzyme is provided, the digestion mode is automatically sequential
    if len(args.enzymes) == 1 and args.mode == 'concurrent':
        args.mode = 'sequential'

    # check that the number of CPUs allocated is not higher than the the number of CPUs of the machine
    if (args.cpu > os.cpu_count()):
        args.cpu = os.cpu_count()

    # write config file with all options for each part
    config = dict()
    config['general'] = {
    'cpu': args.cpu,
    'directory': args.outdir,
    'log': 'log.txt',
    'final_output': 'final_output.tsv'
    }
    config['input'] = {
    'input_file': args.input,
    'input_file_type': args.input_type
    }
    config['blastdb'] = {}
    config['alignment'] = {
    'evalue': args.evalue,
    'id_threshold': args.id_threshold,
    'pos_threshold': args.pos_threshold,
    'raw_output': 'aligned_peptides.xml',
    'filtered_output': 'aligned_peptides_filtered.tsv'
    }
    config['QSAR'] = {
    'DPPIV_file_1': coeff_DPPIV_1_file,
    'DPPIV_file_2': coeff_DPPIV_2_file,
    'ACE_file': coeff_ACE_file,
    'qsar_output': 'QSAR_output.tsv'
    }
    if (args.exp and os.path.isfile(args.exp)):
        config['comparison'] = {
        'comparison_output': 'final_filtered_peptides.tsv',
        'experimental_peptides': args.exp
        }

    step = 0


    # download sequences if a list of identifiers is given
    step += 1
    config['input']['directory'] = f'{step}-input'
    if args.input_type != 'Peaks':  # digestion needed
        if args.input_type == 'identifiers':
            print("*** " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Download sequences from their identifiers")
            config['input']['input_file'] = args.input
            config['input']['identifier_type'] = args.id_type
            config['input']['downloaded_proteins'] = 'proteins_sequences.fasta'
            download(config)

        config['input']['cleaned_proteins'] = 'cleaned_proteins.fasta'

        # perform digestion
        print("*** " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Perform digestion")
        step += 1
        config['digestion'] = {
        'rpg_output': 'digested_peptides_raw.tsv',
        'peptides_fasta': 'digested_peptides.fasta',
        'peptides_tsv': 'digested_peptides.tsv',
        'mode': args.mode,
        'enzymes': args.enzymes,
        'misscleavage': args.misscleavage,
        'directory': f'{step}-digestion'
        }
        digest(config)
    else:   # we provide one or several peptid files from Peaks -> no digestion needed
        config['input']['peptides_fasta'] = 'peptides.fasta'
        config['input']['PTMs_file'] = 'peptides_with_PTMs.tsv'
        read_peptides(config)


    # if we provide fasta file(s), convert them to blastp db
    if not args.database:
        print("*** " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Create blast database")
        step += 1
        config['blastdb']['fasta_db'] = args.fasta
        config['blastdb']['db_name'] = 'complete_db'
        config['blastdb']['directory'] = f'{step}-blastdb'
        create_blast_db(config)
    else:
        config['blastdb']['db_name'] = args.database
        config['blastdb']['directory'] = ''

    # perform blast
    print("*** " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Perform alignment")
    step += 1
    config['alignment']['directory'] = f'{step}-alignment'
    perform_alignment(config)

    # perform QSAR analysis
    print("*** " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Perform QSAR analysis")
    step += 1
    config['QSAR']['directory'] = f'{step}-QSAR'
    perform_qsar_analyses(config)

    # summarise the results of blast and QSAR in one tab file
    print("*** " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Summarise results")
    summarise_results(config)

    # if a file with experimental peptides is given:
    if (args.exp and os.path.isfile(args.exp)):
        print("*** " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Filter with experimental peptides")
        compare_peptides_files(config)

    config_obj = configparser.ConfigParser()
    config_obj.add_section("general")
    config_obj.add_section("input")
    config_obj.add_section("digestion")
    config_obj.add_section("blastdb")
    config_obj.add_section("alignment")
    config_obj.add_section("QSAR")
    if (args.exp and os.path.isfile(args.exp)):
        config_obj.add_section("comparison")
    for section in config:
        for key in config[section]:
            config_obj.set(section, key, str(config[section][key]))

    configfile = os.path.join( args.outdir, 'config.ini' )
    with open(configfile, 'w') as configfile_fh:
        config_obj.write(configfile_fh)

    print("*** " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Done.")


if __name__ == '__main__':
    main()
    sys.exit(0)
