#! /usr/bin/env python

import os
import sys
import argparse
import subprocess
import shutil
import tempfile
import configparser

from datetime import datetime
from Bio import SeqIO

from .digestedpeptide import *
from .utils import *


def digest(config):

    indir = os.path.join( config['general']['directory'], config['input']['directory'] )
    outdir = os.path.join( config['general']['directory'], config['digestion']['directory'] )
    os.makedirs(indir, exist_ok = True)
    os.makedirs(outdir, exist_ok = True)

    nb_cpus = config['general']['cpu']

    if 'downloaded_proteins' in config['input']:
        proteins = [os.path.join( indir, config['input']['downloaded_proteins'] )]
    else:
        proteins = config['input']['input_file']
    mode = config['digestion']['mode']
    enzymes = config['digestion']['enzymes']
    misscleavage = config['digestion']['misscleavage']

    tmp_fh = tempfile.NamedTemporaryFile()
    unq_input = os.path.join( indir, config['input']['cleaned_proteins'] )

    # if several proteins files, concatenate them
    if(len(proteins)>1):
        concatenate_fasta(proteins, tmp_fh.name)
    else:
        tmp_fh.name = proteins[0]

    # check that there is no duplicated entries
    clean_fasta(tmp_fh.name, unq_input)

    peptides_dict = dict()
    complete_peptides_dict = dict()

    if mode == 'sequential':    # sequential mode
        # run rpg
        rpg_out_name = os.path.join( outdir, config['digestion']['rpg_output'] )
        call_rpg(rpg_out_name, unq_input, enzymes, nb_cpus)

        # read rpg output
        peptides_dict = store_peptides_from_rpg(rpg_out_name)

        # add the misscleavages
        complete_peptides_dict = add_misscleaved_peptides(peptides_dict, misscleavage)

        # delete peptides of length 1
        complete_peptides_dict = {elt:value for elt, value in complete_peptides_dict.items() if len(elt) > 1}

        # export peptides in fasta
        peptides_fasta = os.path.join( outdir, config['digestion']['peptides_fasta'] )
        export_as_fasta(complete_peptides_dict, peptides_fasta)

        # export peptides in tsv
        peptides_tsv = os.path.join( outdir, config['digestion']['peptides_tsv'] )
        export_as_tsv_from_fasta(peptides_fasta, peptides_tsv)

    else:              # concurrent mode
        input_for_rpg = unq_input
        count = 1
        tmp_files = list()
        for enz in enzymes:
            # run rpg
            rpg_out_name = os.path.join( outdir, os.path.splitext(config['digestion']['rpg_output'])[0] + '_rpg_tmp_' + str(count) + '.tsv' )
            tmp_files.append(rpg_out_name)
            call_rpg(rpg_out_name, input_for_rpg, [enz], nb_cpus)

            # read rpg output
            peptides_dict = store_peptides_from_rpg(rpg_out_name)

            # add the misscleavages
            complete_peptides_dict = add_misscleaved_peptides(peptides_dict, misscleavage)

            # delete peptides of length 1
            complete_peptides_dict = {elt:value for elt, value in complete_peptides_dict.items() if len(elt) > 1}

            # store peptides in fasta file for next rpg run
            peptides_fasta = os.path.join( outdir, os.path.splitext(config['digestion']['rpg_output'])[0] + '_peptides_tmp_' + str(count) + '.fasta' )
            tmp_files.append(peptides_fasta)
            export_as_fasta_c(complete_peptides_dict, peptides_fasta, count)

            # set the fasta file that will be used in next rpg run
            input_for_rpg = peptides_fasta
            count += 1

        # remove temporary files
        for f in tmp_files[:-2]:
            os.remove(f)

        # rename final rpg output file
        final_rpg_file = os.path.join( outdir, config['digestion']['rpg_output'] )
        os.rename(rpg_out_name, final_rpg_file)

        # rename final digested peptides fasta file so that blast can find it
        final_peptides_fasta = os.path.join( outdir, config['digestion']['peptides_fasta'] )
        os.rename(peptides_fasta, final_peptides_fasta)

        # export peptides in tsv format from the fasta file
        peptides_tsv = os.path.join( outdir, config['digestion']['peptides_tsv'] )
        export_as_tsv_from_fasta(final_peptides_fasta, peptides_tsv)


def call_rpg(outname, proteins, enzymes, cpu):
    enzymes_str = [str(x) for x in enzymes]
    misscleavages = " ".join(["0"] * len(enzymes))
    rpg_cmd = "rpg" \
            + " -c " + str(cpu) \
            + " -d s" \
            + " -e " + " ".join(enzymes_str) \
            + " -m " + misscleavages \
            + " -i " + proteins \
            + " -o " + outname \
            + " -f " + "tsv" \
            + " -n"
    os.system(rpg_cmd)


def store_peptides_from_rpg(rpg_output):
    """Read rpg tsv output file and return a dict

    Input:
        rpg_output (str): path and name of rpg output file

    Output:
        seq_array_by_protein (dict):
            key: protein header
            value (dict):
                key: enzyme
                value: list of peptides sequences obtained
    """
    seq_array_by_protein = dict()
    with open(rpg_output, 'r') as rpg_fh:
        for line in rpg_fh:
            field = line.strip().split("\t")
            if field[0] != "Original_header":
                # field[0]: sequence header
                # field[2]: enzyme
                # field[7]: peptide sequence
                if field[0] not in seq_array_by_protein:
                    seq_array_by_protein[field[0]] = dict()
                if field[2] not in seq_array_by_protein[field[0]]:
                   seq_array_by_protein[field[0]][field[2]]  = list()
                seq_array_by_protein[field[0]][field[2]].append(field[7])
    return seq_array_by_protein


def add_misscleaved_peptides(pep_dict, misscleavage):
    """From a dict of peptides, return a dict of peptides with misscleavages

    Input:
        pep_dict (dict): out put of function 'store_peptides_from_rpg'
        misscleavage (int): maximum number of misscleavages allowed in a peptide

    Output:
        complete_dict (dict):
            key: peptide sequence
            value: set of identifiers
            Each identifier contains the following informations:
            protein ID:peptide start-peptide end|enzyme|nb of misscleavages
    """
    complete_dict = dict()
    for protein in pep_dict:
        for enzyme in pep_dict[protein]:
            start_array = []
            end_array = []
            start = 1
            end = 0
            for i, pep in enumerate(pep_dict[protein][enzyme]):
                end += len(pep)

                if pep not in complete_dict:
                    complete_dict[pep] = set()
                complete_dict[pep].add("{}:{}-{}|{}|0".format(protein, start, end, enzyme))

                start_array.append(start)
                end_array.append(end)

                # deal with misscleavages
                if misscleavage > 0 and i > 0:
                    current_pep = DigestedPeptide(pep, start, end)

                    for j in range(i-1, max(-1, i - misscleavage - 1), -1):
                        previous_pep = DigestedPeptide(pep_dict[protein][enzyme][j], start_array[j], end_array[j])
                        current_pep = concat_peptides(current_pep, previous_pep)

                        if current_pep.sequence not in complete_dict:
                            complete_dict[current_pep.sequence] = set()
                        complete_dict[current_pep.sequence].add("{}:{}-{}|{}|{}".format(protein, current_pep.start, current_pep.end, enzyme, (i-j)))

                # prepare for the next peptide
                start = end + 1
    return complete_dict


def export_as_fasta_c(peptides_dict, output_file, count):
    headers_dict = dict()
    with open(output_file, 'w') as fh:
        for pep in peptides_dict:
            for pos in peptides_dict[pep]:
                if count == 1:
                    headers_dict[pos] = pep
                else:
                    # pos contains something like this:
                    # 'sp|P02070|HBB_BOVIN Hemoglobin subunit beta:92-97|Asp-N|1m:1-6|Caspase-1|0'
                    # pos.split(":")[0] : protein ID
                    # pos.split(":")[1:] : list of cut by enzyme ['92-97|Asp-N|1m', '1-6|Caspase-1|0']
                    prot_ID = ":".join(pos.split(":")[:-2])
                    if len(pos.split(":")[1:]) > 1:     # there is several ":" in the string
                        # first position is absolute (=relative to the global protein)
                        first_start = pos.split(":")[1:][-2].split('|')[0].split('-')[0]
                        first_end   = pos.split(":")[1:][-2].split('|')[0].split('-')[1]
                        first_enz   = pos.split(":")[1:][-2].split('|')[1]
                        first_miss  = pos.split(":")[1:][-2].split('|')[2]

                        # second position is relative (to the peptide described in first position)
                        second_start = pos.split(":")[1:][-1].split('|')[0].split('-')[0]
                        second_end   = pos.split(":")[1:][-1].split('|')[0].split('-')[1]
                        second_enz   = pos.split(":")[1:][-1].split('|')[1]
                        second_miss  = pos.split(":")[1:][-1].split('|')[2]

                        new_enz = ''
                        new_miss = ''
                        abs_start = first_start
                        abs_end = first_end
                        first_pep_length = int(first_end) - int(first_start)
                        second_pep_length = int(second_end) - int(second_start)
                        if first_pep_length == second_pep_length:
                            new_enz = first_enz
                            new_miss = first_miss
                        else:
                            abs_start = int(first_start) + int(second_start) - 1
                            abs_end = int(first_start) + int(second_end) - 1
                            new_enz = first_enz + '+' + second_enz
                            new_miss = first_miss + '+' + second_miss

                        new_pos = prot_ID + ':' + str(abs_start) + '-' + str(abs_end) + '|' + new_enz + '|' + new_miss
                        headers_dict[new_pos] = pep
                    else:
                        print("Does this really happen?")
        positions_detailed = [ [":".join(x.split(":")[:-1])] + x.split(":")[-1].split("|")[0].split("-") + [x.split("|")[-2]] + [x.split("|")[-1]] for x in headers_dict.keys() ]
        positions_detailed.sort(key = lambda positions_detailed: (positions_detailed[0], int(positions_detailed[1]), int(positions_detailed[2]), str(positions_detailed[-1]) ) )
        for pos in positions_detailed:
            new_pos = str(pos[0]) + ":" + str(pos[1]) + "-" + str(pos[2]) + "|" + pos[3] + "|" + pos[4]
            fh.write(">" + new_pos + "\n")
            fh.write(headers_dict[new_pos] + "\n")


def export_as_fasta(peptides_dict, output_file):
    """Export the digested peptides in fasta format.
    Fasta entries are sorted by proteins ID and then start position.

    Input:
        peptides_dict (dict):
            key = peptide sequence
            value = list of headers corresponding to the sequence
            Each header contains the following informations:
            protein ID:peptide start-peptide end|enzyme|nb of misscleavages
        output_file (str): output file path

    Output:
        None
    """
    with open(output_file, 'w') as fh:
        headers_dict = dict()
        for pep in peptides_dict:
            for pos in peptides_dict[pep]:
                headers_dict[pos] = pep
        positions_detailed = [ [':'.join(x.split(":")[0:-1])] + x.split(":")[-1].split("|")[0].split("-") + [x.split("|")[-2]] + [x.split("|")[-1]] for x in headers_dict.keys() ]
        positions_detailed.sort(key = lambda positions_detailed: (positions_detailed[0], int(positions_detailed[1]), int(positions_detailed[2]) ) )
        for pos in positions_detailed:
            new_pos = str(pos[0]) + ":" + str(pos[1]) + "-" + str(pos[2]) + "|" + pos[3] + "|" + pos[4]
            fh.write(">" + new_pos + "\n")
            fh.write(headers_dict[new_pos] + "\n")


def export_as_tsv_from_fasta(fasta_file, tsv_file):
    with open(tsv_file, 'w') as tsv_fh:
        tsv_header = ''
        tsv_header += "Protein" + "\t"
        tsv_header += "Peptide" + "\t"
        tsv_header += "Start" + "\t"
        tsv_header += "End" + "\t"
        tsv_header += "Enzyme" + "\t"
        tsv_header += "Misscleavages" + "\n"
        tsv_fh.write(tsv_header)
        for record in SeqIO.parse(fasta_file, "fasta"):
            protein       = ":".join(record.description.split(":")[:-1])
            start         = record.description.split(":")[-1].split("|")[0].split("-")[0]
            end           = record.description.split(":")[-1].split("|")[0].split("-")[1]
            enzyme        = record.description.split("|")[-2]
            misscleavages = record.description.split("|")[-1]
            tsv_fh.write( protein + "\t")
            tsv_fh.write( str(record.seq) + "\t")
            tsv_fh.write( start + "\t")
            tsv_fh.write( end + "\t")
            tsv_fh.write( enzyme + "\t")
            tsv_fh.write( misscleavages + "\n")



def main():

    begin_time = datetime.now()

    desc = "Enzymatic digestion"

    command = argparse.ArgumentParser(prog = 'digestion.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        help = 'Path to your output directory (default: a folder is created in your input file directory)')

    command.add_argument('-i', '--input', nargs = '+',
        type = str,
        required = True,
        help = 'One or several input proteic fasta file (required)')

    command.add_argument('-d', '--mode', nargs = '?',
        type = str,
        default = 's',
        help = 'Digestion mode. Either ‘s’, ‘sequential’, ‘c’ or ‘concurrent’ (default: s)')

    command.add_argument('-e', '--enzymes', nargs = '+',
        type = int,
        default = [42],
        help = 'Id of enzyme(s) to use (i.e. -e 0 5 10 to use enzymes 0, 5 and 10). Use rpg -l first to get enzyme ids (default 42: trypsin)')

    command.add_argument('-m', '--misscleavage', nargs = '?',
        type = int,
        default = 1,
        help = 'Number of miss-cleavages allowed ([0-3] default 1)')

    command.add_argument('-c', '--cpu', nargs = '?',
        type = int,
        default = 1,
        help = 'Number of CPUs to allocate to rpg and blast (default 1)')

    args = command.parse_args()

    # check options

    if not args.outdir:
        args.outdir = os.path.join( os.path.dirname(args.input[0]), 'digestion_' + begin_time.strftime("%Y%m%d%H%M%S") )

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    if args.mode not in ['s', 'sequential', 'c', 'concurrent']:
        raise Exception("Digestion mode must be either 's', 'sequential', 'c' or 'concurrent'.")

    # this is solely for more clarity in config file
    if args.mode == 's':
        args.mode = 'sequential'
    if args.mode == 'c':
        args.mode = 'concurrent'

    if len(args.enzymes) == 1 and args.mode == 'concurrent':
        args.mode = 'sequential'

    # create config dict with all parameters
    config = dict()
    config['general'] = {
    'cpu': args.cpu,
    'directory': args.outdir
    }
    config['input'] = {
    'directory': '',
    'input_file': args.input,
    'cleaned_proteins': 'cleaned_proteins.fasta'
    }
    config['digestion'] = {
    'directory': '',
    'rpg_output': 'digested_peptides_raw.tsv',
    'peptides_fasta': 'digested_peptides.fasta',
    'peptides_tsv': 'digested_peptides.tsv',
    'mode': args.mode,
    'enzymes': args.enzymes,
    'misscleavage': args.misscleavage
    }

    digest(config)

    # convert config dict to object configparser for the export
    config_obj = configparser.ConfigParser()
    config_obj.add_section("general")
    config_obj.add_section("input")
    config_obj.add_section("digestion")
    for section in config:
        for key in config[section]:
            config_obj.set(section, key, str(config[section][key]))

    configfile = os.path.join( args.outdir, 'config.ini' )
    with open(configfile, 'w') as configfile_fh:
        config_obj.write(configfile_fh)
