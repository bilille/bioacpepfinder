#! /usr/bin/env python

"""
Author: Isabelle GUIGON
Date: 2021-03-05
Developed on Python 3.8
"""

import os
import sys
import argparse
import tempfile
import configparser

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Blast.Applications import NcbimakeblastdbCommandline

from .utils import *

def create_blast_db(config):

    outdir = os.path.join( config['general']['directory'], config['blastdb']['directory'] )
    fasta = config['blastdb']['fasta_db']

    os.makedirs(outdir, exist_ok = True)

    tmp_fh = tempfile.NamedTemporaryFile()
    unq_fasta = os.path.join( outdir, config['blastdb']['db_name'] + ".fasta")

    # concatenate files
    if(len(fasta)>1):
        concatenate_fasta(fasta, tmp_fh.name)
    else:
        tmp_fh.name = fasta[0]

    # check that there is no duplicated entries
    clean_fasta(tmp_fh.name, unq_fasta)

    # call makeblastdb
    unq_db = os.path.join( outdir, config['blastdb']['db_name'])
    call_makeblastdb(unq_fasta, unq_db, os.path.join( config['general']['directory'], config['general']['log']))

    # close tmp file
    tmp_fh.close()


def call_makeblastdb(fasta, name, log_file):
    blastdb_cline = NcbimakeblastdbCommandline(
                    logfile = log_file,
                    input_file = fasta,
                    out = name,
                    dbtype = 'prot',
                    parse_seqids = True)
    try:
        blastdb_cline()
    except:
        raise Exception("Something went wrong with makeblastdb. Please see log file for more information.")


def main():

    desc = "Script to create a blastp db from one or several fasta files"

    command = argparse.ArgumentParser(prog = 'makeblastdb.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-f', '--fasta', nargs = '+',
        type = str,
        required = True,
        help = 'One or several input fasta file (required)')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        required = True,
        help = 'Output directory (required)')

    args = command.parse_args()

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # create config dict with all parameters
    config = dict()
    config['blastdb'] = {
    'directory': '',
    'fasta_db': args.fasta,
    'db_name': 'complete_db'
    }
    config['general'] = {
    'directory': args.outdir,
    'log': 'log.txt'
    }

    create_blast_db(config)

    # convert config dict to object configparser for the export
    config_obj = configparser.ConfigParser()
    config_obj.add_section("general")
    config_obj.add_section("blastdb")
    for section in config:
        for key in config[section]:
            config_obj.set(section, key, str(config[section][key]))

    configfile = os.path.join( args.outdir, 'config.ini' )
    with open(configfile, 'w') as configfile_fh:
        config_obj.write(configfile_fh)
