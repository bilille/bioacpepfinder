#! /usr/bin/env python

import re
import os
import sys
import time
import argparse
import requests
import configparser

from datetime import datetime
from Bio import Entrez
from Bio import SeqIO

from .utils import *


def write_sequence_on_several_lines(sequence, length_line):
    output_sequence = ''
    length = len(sequence)
    nb_iterations = length // length_line
    for i in range(0, nb_iterations):
        output_sequence += sequence[ i*length_line:(i+1)*length_line ] + "\n";
    if nb_iterations*length_line < length:
        output_sequence += sequence[ nb_iterations*length_line:] + "\n"
    return output_sequence


def download(config):

    outdir = os.path.join( config['general']['directory'], config['input']['directory'] )
    id_files = config['input']['input_file']
    id_types = config['input']['identifier_type']

    os.makedirs(outdir, exist_ok = True)

    log_file = os.path.join( config['general']['directory'], config['general']['log'])

    # for each id, determine the type to call the appropriate function(s)
    sequences_file = os.path.join( outdir, config['input']['downloaded_proteins'] )
    with open(log_file, 'w') as log_fh:
        with open(sequences_file, 'w') as fh:
            i = 0
            for id_file in id_files:
                id_type = id_types[i]
                with open(id_file, 'r') as in_fh:
                    for line in in_fh:
                        my_id = line.strip()
                        if id_type == "uniprot":
                            result = download_uniprot_proteins(my_id)
                            if result != '':
                                fh.write(result)
                            else:
                                log_fh.write("WARNING: no fasta sequence found for Uniprot identifier {}.\n".format(my_id))
                            time.sleep(0.5)
                        elif id_type == "ensembl":
                            ensembl_type = guess_ensembl_id_type(my_id)
                            list_protein_ids = list()
                            if ensembl_type == "gene":
                                list_protein_ids = get_ensembl_protein_ids_from_gene_ids( [my_id] )
                            elif ensembl_type == "transcript":
                                list_protein_ids = get_ensembl_protein_ids_from_transcript_ids( [my_id] )
                            elif ensembl_type == "protein":
                                list_protein_ids.append(my_id)
                            else:
                                log_fh.write("WARNING: identifier {} does not look like an Ensembl identifier.\n".format(my_id))
                            if ensembl_type != "unknown" and len(list_protein_ids) == 0:
                                log_fh.write("WARNING: no protein identifiers found from Ensembl identifier {}.\n".format(my_id))
                            for id_prot in list_protein_ids:
                                result = download_ensembl_proteins(id_prot)
                                if result != '':
                                    fh.write(result)
                                else:
                                    log_fh.write("WARNING: no fasta sequence found for Ensembl identifier {}.\n".format(id_prot))
                                time.sleep(0.5)
                        elif id_type == "ncbi":
                            result = download_ncbi_proteins(my_id)
                            if result != '':
                                fh.write(result)
                            else:
                                log_fh.write("WARNING: no fasta sequence found for NCBI identifier {}.\n".format(my_id))
                            time.sleep(0.5)
                        else:
                            raise Exception("Unimplemented type")
                i += 1
    if count_fasta_sequences(sequences_file) == 0:
        raise Exception("FATAL: no fasta sequence has been downloaded from the given identifiers.")


def guess_ensembl_id_type(my_id):
    gene_regex = re.compile('ENS[A-Z]{3}G[\d]+')
    human_gene_regex = re.compile('ENSG[\d]+')
    transcript_regex = re.compile('ENS[A-Z]{3}T[\d]+')
    human_transcript_regex = re.compile('ENST[\d]+')
    prot_regex = re.compile('ENS[A-Z]{3}P[\d]+')
    human_prot_regex = re.compile('ENSP[\d]+')
    if gene_regex.match(my_id) or human_gene_regex.match(my_id):
        return "gene"
    elif transcript_regex.match(my_id) or human_transcript_regex.match(my_id):
        return "transcript"
    elif prot_regex.match(my_id) or human_prot_regex.match(my_id):
        return "protein"
    else:
        return "unknown"


def get_ensembl_protein_ids_from_gene_ids(gene_ids):
    protein_ids = set()
    server = "https://rest.ensembl.org"
    for gene_id in gene_ids:
        ext = "/lookup/id/" + gene_id + "?expand=1"
        r = requests.get(server+ext, headers={ "Content-Type" : "application/json"})
        if r.ok:
            decoded = r.json()
            for transcript in decoded['Transcript']:
                try:
                    protein_ids.add(transcript['Translation']['id'])
                except:
                    pass
    return protein_ids


def get_ensembl_protein_ids_from_transcript_ids(transcript_ids):
    protein_ids = set()
    server = "https://rest.ensembl.org"
    for transcript_id in transcript_ids:
        ext = "/lookup/id/" + transcript_id + "?expand=1"
        r = requests.get(server+ext, headers={ "Content-Type" : "application/json"})
        if r.ok:
            decoded = r.json()
            try:
                protein_ids.add(decoded['Translation']['id'])
            except:
                pass
    return protein_ids


def download_uniprot_proteins(id_prot):
    base_url = "https://www.uniprot.org/uniprot/"
    complete_url = base_url + id_prot + ".fasta"
    response = requests.post(complete_url, timeout = 0.5)
    if response.status_code == requests.codes.ok:
        return response.text
    return ''


def download_ensembl_proteins(id_prot):
    server = "https://rest.ensembl.org"
    ext = "/sequence/id"
    headers={ "Content-Type" : "application/json", "Accept" : "application/json"}
    data = '{ "ids" : ["' + id_prot + '" ] }'
    r = requests.post(server+ext, headers=headers, data=data)
    if r.ok:
        decoded = r.json()
        return ">" + decoded[0]["id"] + "\n" + write_sequence_on_several_lines(decoded[0]["seq"], 60) + "\n"
    return ''


def download_ncbi_proteins(id_prot):
    Entrez.email = "A.N.Other@example.com"
    result = ''
    try:
        ncbi_handle = Entrez.efetch(db='protein',id=id_prot, rettype = 'fasta', retmode= 'text')
        result = ncbi_handle.read()
        ncbi_handle.close()
    except:
        pass
    return result


def main():

    begin_time = datetime.now()

    desc = "REST API for downloading protein sequences"

    command = argparse.ArgumentParser(prog = 'download_proteins.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-i', '--input', nargs = '+',
        type = str,
        required = True,
        help = 'List of identifiers for proteins / genes to be downloaded (required)')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        help = 'Path to your output directory (default: a folder is created in your input file directory)')

    command.add_argument('-t', '--type', nargs = '+',
        type = str,
        default = ['uniprot'],
        help = "Type of identifier ({'uniprot', 'ensembl', 'ncbi'} default: uniprot")

    args = command.parse_args()

    if not args.outdir:
        args.outdir = os.path.join( os.path.dirname(args.input[0]), 'sequences_' + begin_time.strftime("%Y%m%d%H%M%S") )

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # create config dict with all parameters
    config = dict()
    config['general'] = {
    'directory': args.outdir,
    'log': 'log.txt'
    }
    config['input'] = {
    'input_file': args.input,
    'identifier_type': args.type,
    'directory': '',
    'downloaded_proteins': 'proteins_sequences.fasta'
    }

    download(config)

    # convert config dict to object configparser for the export
    config_obj = configparser.ConfigParser()
    config_obj.add_section("general")
    config_obj.add_section("input")
    for section in config:
        for key in config[section]:
            config_obj.set(section, key, str(config[section][key]))

    configfile = os.path.join( args.outdir, 'config.ini' )
    with open(configfile, 'w') as configfile_fh:
        config_obj.write(configfile_fh)
