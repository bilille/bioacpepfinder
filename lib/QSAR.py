#! /usr/bin/env python

import os
import sys
import argparse
import configparser

from datetime import datetime
from Bio import SeqIO


def perform_qsar_analyses(config):

    outdir = os.path.join( config['general']['directory'], config['QSAR']['directory'] )
    if 'digestion' in config:
        input_file = os.path.join( config['general']['directory'], config['digestion']['directory'], config['digestion']['peptides_fasta'] )
    else:
        input_file =  os.path.join( config['general']['directory'], config['input']['directory'], config['input']['peptides_fasta'] )

    DPPIV_file_1 = config['QSAR']['DPPIV_file_1']
    DPPIV_file_2 = config['QSAR']['DPPIV_file_2']
    ACE_file = config['QSAR']['ACE_file']

    os.makedirs(outdir, exist_ok = True)

    qsar_output = os.path.join( outdir, config['QSAR']['qsar_output'] )

    # store coefficients
    coeff_DPPIV_1 = read_coefficients_file(DPPIV_file_1)
    coeff_DPPIV_2 = read_coefficients_file(DPPIV_file_2)
    coeff_ACE = read_coefficients_file(ACE_file)

    # analyse each peptide
    with open(qsar_output, 'w') as out_fh:
        header = ""
        header += "peptide" + "\t"
        header += "sequence" + "\t"
        header += "DPPIV_features_score" + "\t"
        header += "DPPIV_zScore_IC50" + "\t"
        header += "DPPIV_vScore_IC50" + "\t"
        header += "ACE_zScore_IC50" + "\t"
        header += "comment" + "\n"
        out_fh.write(header)
        for record in SeqIO.parse(input_file, "fasta"):
            out_fh.write(record.description + "\t")
            out_fh.write(str(record.seq) + "\t")

            ## DPPIV
            zScore_dppiv = 0
            vScore_dppiv = 0
            # calculate DPPIV features score (if 0 it's useless to calculate the zScore and vScore)
            score_features_dppiv = calculate_score_features_dppiv(record.seq)
            out_fh.write( str(score_features_dppiv) + "\t" )
            if score_features_dppiv > 0:    # calculate DPPIV zScore and vScore
                zScore_dppiv = calculate_Score_dppiv(record.seq, '3z_z', coeff_DPPIV_1, coeff_DPPIV_2)
                vScore_dppiv = calculate_Score_dppiv(record.seq, '3v_v', coeff_DPPIV_1, coeff_DPPIV_2)

            ## ACE
            zScore_ace = 0
            if len(record.seq) < 7:
                zScore_ace = calculate_Score_ace(record.seq, coeff_ACE)

            out_fh.write( str(zScore_dppiv) + "\t" )
            out_fh.write( str(vScore_dppiv) + "\t" )
            out_fh.write( str(zScore_ace) + "\t" )

            for AA in ['X', 'B', 'Z', 'J']:
                special_AA = set()
                if AA in [record.seq[0], record.seq[1], record.seq[-1], record.seq[-2]]:
                    special_AA.add(AA)
                if special_AA:
                    out_fh.write(f'Special AA in sequence: {",".join(special_AA)}')
            out_fh.write("\n")


def read_coefficients_file(coeff_file):
    coeff_dict = dict()
    header_col = dict()
    with open(coeff_file, 'r') as in_fh:
        header = in_fh.readline().rstrip()
        header_fields = header.split("\t")
        for i, x in enumerate(header_fields):
            header_col[i] = x
        for line in iter(in_fh.readline, ''):
            fields = line.rstrip().split("\t")
            for i, elt in enumerate(fields):
                if i != 0:
                    if fields[0] not in coeff_dict:
                        coeff_dict[fields[0]] = dict()
                    coeff_dict[fields[0]][header_col[i]] = float(elt)
    return coeff_dict


def calculate_score_features_dppiv(seq):
    score = 0
    if seq[0] in ['F', 'I', 'L', 'W']:
        score += 1
    if seq[1] in ['P', 'A']:
        score += 1
    if seq[-1] == 'P':
        score += 1
    return score


def calculate_Score_dppiv(seq, score_type, coeff_DPPIV_1, coeff_DPPIV_2):
    # score_type can be "3z_z", "3v_v", "5z_z"
    score = 0
    score += coeff_DPPIV_2['C'][score_type[-1] + 'Score']
    score += coeff_DPPIV_2['V1N1'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[0]][score_type + '1']
    score += coeff_DPPIV_2['V2N1'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[0]][score_type + '2']
    score += coeff_DPPIV_2['V3N1'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[0]][score_type + '3']
    score += coeff_DPPIV_2['V1N2'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[1]][score_type + '1']
    score += coeff_DPPIV_2['V2N2'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[1]][score_type + '2']
    score += coeff_DPPIV_2['V3N2'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[1]][score_type + '3']
    score += coeff_DPPIV_2['V1C1'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[-1]][score_type + '1']
    score += coeff_DPPIV_2['V2C1'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[-1]][score_type + '2']
    score += coeff_DPPIV_2['V3C1'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[-1]][score_type + '3']
    score += coeff_DPPIV_2['V1C2'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[-2]][score_type + '1']
    score += coeff_DPPIV_2['V2C2'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[-2]][score_type + '2']
    score += coeff_DPPIV_2['V3C2'][score_type[-1] + 'Score'] * coeff_DPPIV_1[seq[-2]][score_type + '3']
    return score


def calculate_Score_ace(seq, coeff_ACE):
    score = 0
    logScore = 2.21
    logScore += 0.03 * coeff_ACE[seq[-1]]['3z25_z1']
    logScore -= 0.10 * coeff_ACE[seq[-1]]['3z25_z2']
    logScore -= 0.21 * coeff_ACE[seq[-1]]['3z25_z3']
    logScore -= 0.07 * coeff_ACE[seq[-2]]['3z25_z1']
    logScore += 0.12 * coeff_ACE[seq[-2]]['3z25_z2']
    logScore -= 0.17 * coeff_ACE[seq[-2]]['3z25_z3']
    score = 10**logScore
    return score


def main():

    begin_time = datetime.now()

    desc = "QSAR analysis of bioactivity"

    command = argparse.ArgumentParser(prog = 'QSAR.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        help = 'Path to your output directory (default: a folder is created in your input file directory)')

    command.add_argument('-p', '--peptides', nargs = '?',
        type = str,
        required = True,
        help = 'One or several input proteic fasta file (required)')

    command.add_argument('--coeff_DPPIV_1', nargs = '?',
        type = str,
        required = True,
        help = 'Tabulated file with coefficients for zScore and vScore for DPPIV (table1) (required)')

    command.add_argument('--coeff_DPPIV_2', nargs = '?',
        type = str,
        required = True,
        help = 'Tabulated file with coefficients for zScore and vScore for DPPIV (table2) (required)')

    command.add_argument('--coeff_ACE', nargs = '?',
        type = str,
        required = True,
        help = 'Tabulated file with coefficients for zScore and vScore for ACE (required)')

    args = command.parse_args()

    if not args.outdir:
        args.outdir = os.path.join( os.path.dirname(args.peptides), 'QSAR_' + begin_time.strftime("%Y%m%d%H%M%S") )

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # create config dict with all parameters
    config = dict()
    config['general'] = {
    'directory': args.outdir
    }
    config['digestion'] = {
    'directory': '',
    'peptides_fasta': args.peptides
    }
    config['QSAR'] = {
    'directory': '',
    'DPPIV_file_1': args.coeff_DPPIV_1,
    'DPPIV_file_2': args.coeff_DPPIV_2,
    'ACE_file': args.coeff_ACE,
    'qsar_output': 'QSAR_output.tsv'
    }

    perform_qsar_analyses(config)

    # convert config dict to object configparser for the export
    config_obj = configparser.ConfigParser()
    config_obj.add_section("general")
    config_obj.add_section("digestion")
    config_obj.add_section("QSAR")
    for section in config:
        for key in config[section]:
            config_obj.set(section, key, str(config[section][key]))

    configfile = os.path.join( args.outdir, 'config.ini' )
    with open(configfile, 'w') as configfile_fh:
        config_obj.write(configfile_fh)


if __name__ == '__main__':
    main()
    sys.exit(0)
