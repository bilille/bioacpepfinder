#! /usr/bin/env python3

import os
import sys
import argparse
import configparser

from datetime import datetime


def compare_peptides_files(config):

    outdir = config['general']['directory']
    exp_file = config['comparison']['experimental_peptides']

    if 'theoretical_peptides' in config['comparison']:
        theo_file = config['comparison']['theoretical_peptides']
    else:
        theo_file = os.path.join( config['general']['directory'], config['general']['final_output'] )

    exp_peptides_list = read_exp_file(exp_file)

    output_file_name = os.path.join( outdir, config['comparison']['comparison_output'] )
    with open(output_file_name, 'w') as out_fh:
        with open(theo_file, 'r') as in_fh:
            for line in in_fh.readlines():
                field = line.split("\t")
                if field[0] == "peptide":
                    out_fh.write(line)
                if field[3] in exp_peptides_list:
                    out_fh.write(line)


def read_exp_file(exp_file):
    peptide_list = list()
    with open(exp_file, 'r') as in_fh:
        for line in in_fh.readlines():
            if line[0] != "#":
                peptide_list.append(line.strip())
    return peptide_list


def main():

    begin_time = datetime.now()

    desc = "Comparison of lists of peptides"

    command = argparse.ArgumentParser(prog = 'peptide_lists_comparison.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        help = 'Path to your output directory (default: a folder is created in your input file directory)')

    command.add_argument('-e', '--exp', nargs = '?',
        type = str,
        required = True,
        help = 'A list of experimental peptides (one peptide per line) (required)')

    command.add_argument('-t', '--theo', nargs = '?',
        type = str,
        required = True,
        help = 'A list of theoretical peptides (BioacPepFinder.py output file) (required)')

    args = command.parse_args()

    if not args.outdir:
        args.outdir = os.path.join( os.path.dirname(args.exp), 'comparison_' + begin_time.strftime("%Y%m%d%H%M%S") )

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # create config dict with all parameters
    config = dict()
    config['general'] = {
    'directory': args.outdir
    }
    config['comparison'] = {
    'theoretical_peptides': args.theo,
    'experimental_peptides': args.exp,
    'comparison_output': 'final_filtered_peptides.tsv'
    }

    compare_peptides_files(config)

    # convert config dict to object configparser for the export
    config_obj = configparser.ConfigParser()
    config_obj.add_section("general")
    config_obj.add_section("comparison")
    for section in config:
        for key in config[section]:
            config_obj.set(section, key, str(config[section][key]))

    configfile = os.path.join( args.outdir, 'config.ini' )
    with open(configfile, 'w') as configfile_fh:
        config_obj.write(configfile_fh)
