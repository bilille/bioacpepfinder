#! /usr/bin/env python

"""
Author: Isabelle GUIGON
Date: 2021-03-05
Developed on Python 3.8

BIOPEP file must be a tabulated file and must contain at least 3 columns
with the following header names: "ID", "Activity" and "Sequence"
(no matter the columns order).
"""

import os
import sys
import argparse


def main():
    desc = "Convert Biopep data to fasta"

    command = argparse.ArgumentParser(prog = 'biopep_to_fasta.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-i', '--input', nargs = '?',
        type = argparse.FileType('r'),
        required = True,
        help = 'Biopep data in TSV format (required)')

    command.add_argument('-o', '--output', nargs = '?',
        type = str,
        help = 'Output file name (default: "biopep.fasta")')

    args = command.parse_args()

    if not args.output:
        args.output = os.path.join( os.path.dirname(args.input.name), 'biopep.fasta' )

    header = args.input.readline().strip()
    header_fields = header.split("\t")
    header_fields = [x.strip() for x in header_fields]
    id_col = -1
    activity_col = -1
    sequence_col = -1
    for i, field in enumerate(header_fields):
        if field == "ID": id_col = i
        if field == "Activity": activity_col = i
        if field == "Sequence": sequence_col = i

    with open(args.output, 'w') as out_fh:
        for line in iter(args.input.readline, ''):
            fields = line.strip().split("\t")
            fields = [x.strip() for x in fields]
            out_fh.write(">BIOPEP|" + fields[id_col] + "|" + fields[activity_col] + "\n")
            out_fh.write(fields[sequence_col] + "\n")


if __name__ == '__main__':
    main()
    sys.exit(0)
