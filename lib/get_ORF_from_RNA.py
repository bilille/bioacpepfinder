#! /usr/bin/env python

import os
import sys
import argparse
import tempfile
import configparser

from datetime import datetime
from Bio import SeqIO

from .utils import *


def get_ORFs(config):

    indir = os.path.join( config['general']['directory'], config['input']['directory'] )
    outdir = os.path.join( config['general']['directory'], config['ORFs']['directory'] )
    os.makedirs(indir, exist_ok = True)
    os.makedirs(outdir, exist_ok = True)

    # concatenate and clean input fasta
    tmp_fh = tempfile.NamedTemporaryFile()
    unq_input = os.path.join(indir, config['input']['cleaned_mRNAs'])
    if(len(config['input']['input_file'])>1):
        concatenate_fasta(config['input']['input_file'], tmp_fh.name)
    else:
        tmp_fh.name = config['input']['input_file'][0]
    clean_fasta(tmp_fh.name, unq_input)

    output_file = os.path.join( outdir, config['ORFs']['ORFs_file'] )
    with open(output_file, 'w') as out_fh:
        for record in SeqIO.parse(unq_input, "fasta"):
            results = find_ORFs(record, config['ORFs']['genetic_code'], config['ORFs']['min_ORF_length'])
            for seq_id, strand, frame, start, end, seq in results:
                out_fh.write(f'>{seq_id}:{start}-{end}:{strand}:{frame}\n')
                out_fh.write(f'{seq}\n')


def find_ORFs(record, trans_table, min_protein_length):
    results = []
    for strand, nuc in [(+1, record.seq), (-1, record.seq.reverse_complement())]:
        for frame in range(3):
            length = 3 * ((len(record.seq)-frame) // 3)
            protein = nuc[frame:frame+length].translate(trans_table)
            starts_pos = []
            start = 0
            end = 0
            for i, AA in enumerate(protein):
                if AA == 'M':
                    starts_pos.append(i)
                if AA == '*':
                    end = i
                    for start in starts_pos:    # starts_pos is empty if we met no 'M'
                        start_DNA = 3 * start + frame + 1   # start inclus
                        end_DNA = 3 * end + frame           # end inclus
                        results.append([record.description, strand, frame, start_DNA, end_DNA, protein[start:end]])
                    # prepare for next peptide
                    starts_pos = []
    return results


def main():
    
    begin_time = datetime.now()

    desc = "Find ORF from mature mRNA (without exons)"

    command = argparse.ArgumentParser(prog = 'get_ORF_from_RNA.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-i', '--input', nargs = '+',
        type = str,
        required = True,
        help = 'Input RNA fasta file(s) (required)')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        help = 'Path to your output directory (default: a folder is created in your input file directory)')

    command.add_argument('--min_orf_length', nargs = '?',
        default = 1,
        help = 'Minimum length to keep an ORF (default 1)')

    args = command.parse_args()

    # check arguments
    if not args.outdir:
        args.outdir = os.path.join( os.path.dirname(args.input[0]), 'ORF_' + begin_time.strftime("%Y%m%d%H%M%S") )

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # create config dict with all parameters
    config = dict()
    config['general'] = {
    'directory': args.outdir
    }
    config['input'] = {
    'directory': '',
    'input_file': args.input,
    'cleaned_mRNAs': 'cleaned_mRNAs.fasta'    
    }
    config['ORFs'] = {
    'directory': '',
    'ORFs_file': 'ORFs.fasta',
    'min_ORF_length': args.min_orf_length,
    'genetic_code': 1
    }

    get_ORFs(config)

    # convert config dict to object configparser for the export
    config_obj = configparser.ConfigParser()
    config_obj.add_section("general")
    config_obj.add_section("input")
    config_obj.add_section("ORFs")
    for section in config:
        for key in config[section]:
            config_obj.set(section, key, str(config[section][key]))

    configfile = os.path.join( args.outdir, 'config.ini' )
    with open(configfile, 'w') as configfile_fh:
        config_obj.write(configfile_fh)
