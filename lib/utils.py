"""Useful functions that cannot be added to a class"""

import os
import re
import configparser

from Bio import SeqIO
from Bio.Seq import Seq

from .digestedpeptide import *


def concat_peptides(peptide1, peptide2):
    """Function to concatenate 2 joining objects DigestedPeptide.
    The peptides must not be overlapping.
    There must not be any gap between the peptides.
    We can only concatenate them if
        peptide1.end == peptide2.start
        or peptide1.end == peptide2.start - 1
    """
    if peptide1.start > peptide2.start:
        peptide1, peptide2 = peptide2, peptide1

    if peptide1.start == peptide2.start or peptide1.end == peptide2.end:
        raise ValueError("Cannot concatenate peptides if one of them is included in the other")

    if peptide1.end > peptide2.start:
        raise ValueError("Cannot concatenate peptides if one of them is included in the other")

    if peptide2.start - peptide1.end > 1:
        raise ValueError("Cannot concatenate peptides if there is a gap between them (peptide 1: {} ; peptide 2: {}".format(peptide1, peptide2))

    if peptide1.end == peptide2.start:
        new_seq = peptide1.sequence[:-1] + peptide2.sequence
    else:
        new_seq = peptide1.sequence + peptide2.sequence

    new_start = peptide1.start
    new_end = peptide2.end

    return DigestedPeptide(new_seq, new_start, new_end)


def concatenate_fasta(files, result):
    cat_cmd = "cat " + " ".join(files) + " > " + result
    os.system(cat_cmd)


def clean_fasta(input_fasta, output_fasta):
    """This function removes all non-alphanumerical characters in sequences
    and check that there are no duplicated ID associated with different sequences."""
    regex_nonalphanumerical = re.compile(r'\W')
    with open(output_fasta, 'w') as out_fh:
        dict_seq = dict()
        for record in SeqIO.parse(input_fasta, "fasta"):
            if record.id in dict_seq:
                if record.seq != dict_seq[record.id]:
                    print("WARNING: inconsistent data. {} is associated with different sequences.".format(record.id))
            else:
                record.seq = Seq(regex_nonalphanumerical.sub(r'', str(record.seq)))
                dict_seq[record.id] = record.seq
                out_fh.write(record.format("fasta"))


def count_fasta_sequences(input_fasta):
    nb_sequences = 0
    with open(input_fasta, 'r') as in_fh:
        fasta_sequences = SeqIO.parse(in_fh,'fasta')
        for fasta in fasta_sequences:
            nb_sequences += 1
    return nb_sequences


def summarise_results(config):

    outdir = config['general']['directory']
    alignment_dir = os.path.join( outdir, config['alignment']['directory'] )
    qsar_dir = os.path.join( outdir, config['QSAR']['directory'] )

    alignment_file = os.path.join( alignment_dir, config['alignment']['filtered_output'] )
    qsar_file = os.path.join( qsar_dir, config['QSAR']['qsar_output'] )

    # read alignment file
    aln_data = dict()
    with open(alignment_file, 'r') as aln_fh:
        for line in aln_fh:
            field = line.strip().split("\t")
            if field[0] != "Query_ID":
                if field[0] not in aln_data:
                    aln_data[field[0]] = dict()
                    aln_data[field[0]]['target_id'] = list()
                    aln_data[field[0]]['target_seq'] = list()
                    aln_data[field[0]]['aln_length'] = list()
                aln_data[field[0]]['target_id'].append(field[2])
                aln_data[field[0]]['target_seq'].append(field[3])
                aln_data[field[0]]['aln_length'].append(field[4])

    # read QSAR file
    qsar_data = dict()
    with open(qsar_file, 'r') as qsar_fh:
        for line in qsar_fh:
            field = line.strip().split("\t")
            if field[0] != "peptide":
                if field[0] not in qsar_data:
                    qsar_data[field[0]] = dict()
                qsar_data[field[0]]['sequence'] = field[1]
                qsar_data[field[0]]['DPPIV_features_score'] = field[2]
                qsar_data[field[0]]['DPPIV_zScore_IC50'] = field[3]
                qsar_data[field[0]]['DPPIV_vScore_IC50'] = field[4]
                qsar_data[field[0]]['ACE_zScore_IC50'] = field[5]
                qsar_data[field[0]]['comment'] = str((field[6:7]+[''])[0])  # syntax to return '' as default value if the column 6 is empty

    # write output file
    with open( os.path.join( outdir, config['general']['final_output'] ), 'w' ) as f_fh:
        header = 'peptide' + "\t"
        header += 'enzyme' + "\t"
        header += 'misscleavages' + "\t"
        header += 'sequence' + "\t"
        header += 'blast' + "\t"
        header += 'matching_sequence' + "\t"
        header += 'aln_lengths' + "\t"
        header += 'DPPIV_features_score' + "\t"
        header += 'DPPIV_zScore_IC50' + "\t"
        header += 'DPPIV_vScore_IC50' + "\t"
        header += 'ACE_zScore_IC50' + "\t"
        header += "comment" + "\n"
        f_fh.write(header)
        for pep in sorted(qsar_data):
            pep_fields = pep.split('|')
            enzyme = pep_fields[-2]
            misscleavages = pep_fields[-1]
            pep_only = '|'.join(pep_fields[:-2])
            line = "\t".join([pep_only, enzyme, misscleavages]) + "\t"
            line += qsar_data[pep]['sequence'] + "\t"
            if pep in aln_data:
                line += ','.join(aln_data[pep]['target_id']) + "\t"
                if len( list(set(aln_data[pep]['target_seq'])) ) == 1:  # all targets have the same sequence
                    line +=aln_data[pep]['target_seq'][0] + "\t"
                    line +=aln_data[pep]['aln_length'][0] + "\t"
                else:
                    line += ','.join(aln_data[pep]['target_seq']) + "\t"
                    line += ','.join(aln_data[pep]['aln_length']) + "\t"
            else:
                line += '' + "\t" + '' + "\t" + '' + "\t"
            line += qsar_data[pep]['DPPIV_features_score'] + "\t"
            line += qsar_data[pep]['DPPIV_zScore_IC50'] + "\t"
            line += qsar_data[pep]['DPPIV_vScore_IC50'] + "\t"
            line += qsar_data[pep]['ACE_zScore_IC50'] + "\t"
            line += qsar_data[pep]['comment'] + "\n"
            f_fh.write(line)

