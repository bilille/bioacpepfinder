#! /usr/bin/env python

"""
Author: Isabelle GUIGON
Date: 2021-02-16
Developed on Python 3.8
"""

import os
import sys
import argparse
import shutil
from datetime import datetime
import configparser

import Bio
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastpCommandline
from Bio.Blast import NCBIXML


def perform_alignment(config):

    nb_cpus = config['general']['cpu']

    outdir = os.path.join( config['general']['directory'], config['alignment']['directory'] )
    if 'digestion' in config:
        peptides = os.path.join( config['general']['directory'], config['digestion']['directory'], config['digestion']['peptides_fasta'] )
    else:
        peptides =  os.path.join( config['general']['directory'], config['input']['directory'], config['input']['peptides_fasta'] )

    database = os.path.join( config['general']['directory'], config['blastdb']['directory'], config['blastdb']['db_name'] )
    evalue = config['alignment']['evalue']
    id_threshold = config['alignment']['id_threshold']
    pos_threshold = config['alignment']['pos_threshold']

    os.makedirs(outdir, exist_ok = True)

    # run blast
    blast_output = os.path.join( outdir, config['alignment']['raw_output'] )
    run_blastp(blast_output, peptides, database, evalue, nb_cpus)

    # filter blast output
    final_output =  os.path.join( outdir, config['alignment']['filtered_output'] )
    filter_blast_output(blast_output, final_output, id_threshold, pos_threshold)


def run_blastp(outname, peptides, database, evalue, cpu):
    blastp_cmd = NcbiblastpCommandline(
        query=peptides,
        db=database,
        evalue=evalue,
        outfmt=5,
        word_size=4,
        max_target_seqs=500,
        out=outname,
        num_threads=cpu)
    blastp_cmd()


def filter_blast_output(xml_file, output, id_threshold, pos_threshold):
    result_handle = open(xml_file)
    blast_records = NCBIXML.parse(result_handle)
    with open(output, 'w') as out_fh:
        header = ''
        header += 'Query_ID' + "\t"
        header += 'Query_seq' + "\t"
        header += 'Target_ID' + "\t"
        header += 'Target_seq' + "\t"
        header += 'Aln_length' + "\t"
        header += '%_identity' + "\t"
        header += '%_positives' + "\t"
        header += 'Mismatches' + "\t"
        # ~ header += 'Gaps' + "\t"
        header += 'Q_start' + "\t"
        header += 'Q_end' + "\t"
        header += 'T_start' + "\t"
        header += 'T_end' + "\t"
        header += 'Evalue' + "\t"
        header += 'Bitscore' + "\n"
        out_fh.write(header)
        for blast_record in blast_records:
            for alignment in blast_record.alignments:
                for hsp in alignment.hsps:
                    mismatches = hsp.align_length - hsp.identities - hsp.gaps
                    identity = float(100 * hsp.identities) / hsp.align_length
                    positives = float(100 * hsp.positives) / hsp.align_length
                    if identity >= id_threshold and positives >= pos_threshold:
                        line = ''
                        line += blast_record.query + "\t"                # query ID
                        line += hsp.query + "\t"                         # query sequence
                        line += alignment.accession.strip(',') + "\t"    # target ID
                        line += hsp.sbjct + "\t"                         # target sequence
                        line += str(hsp.align_length) + "\t"             # alignment length
                        line += str(identity) + "\t"                     # % identity
                        line += str(positives) + "\t"                    # % positives
                        line += str(mismatches) + "\t"                   # mismatches
                        # ~ line += str(hsp.gaps) + "\t"                 # nb gap
                        line += str(hsp.query_start) + "\t"              # query start
                        line += str(hsp.query_end) + "\t"                # query end
                        line += str(hsp.sbjct_start) + "\t"              # subject start
                        line += str(hsp.sbjct_end) + "\t"                # subject end
                        line += str(hsp.expect) + "\t"                   # evalue
                        line += str( round(hsp.bits, 1) )                # bit score
                        out_fh.write(line + "\n")


def main():

    begin_time = datetime.now()

    desc = "Wrapper for blastp"

    command = argparse.ArgumentParser(prog = 'blastp_wrapper.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        help = 'Path to your output directory (default: a folder is created in your input file directory)')

    command.add_argument('-p', '--peptides', nargs = '?',
        type = str,
        required = True,
        help = 'Input fasta file (required)')

    command.add_argument('-d', '--database', nargs = '?',
        type = str,
        required = True,
        help = 'blast database (required)')

    command.add_argument('-e', '--evalue', nargs = '?',
        type = float,
        default = 0.1,
        help = 'Evalue threshold for blast (default 0.1)')

    command.add_argument('--id_threshold', nargs = '?',
        type = int,
        default = 100,
        help = 'Evalue threshold for percentage of identity (default 100)')

    command.add_argument('--pos_threshold', nargs = '?',
        type = int,
        default = 0,
        help = 'Evalue threshold for percentage of positives (default 0 - we do not filter on positives). --pos_threshold N implies --id_threshold N.')

    command.add_argument('-c', '--cpu', nargs = '?',
        type = int,
        default = 1,
        help = 'Number of CPUs to allocate to rpg and blast (default 1)')

    args = command.parse_args()

    if not args.outdir:
        args.outdir = os.path.join( os.path.dirname(args.peptides), 'blastp_' + begin_time.strftime("%Y%m%d%H%M%S") )

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    if args.pos_threshold != 0:
        args.id_threshold = args.pos_threshold

    # create config dict with all parameters
    config = dict()
    config['general'] = {
    'cpu': args.cpu,
    'directory': args.outdir
    }
    config['digestion'] = {
    'directory': '',
    'peptides_fasta': args.peptides
    }
    config['blastdb'] = {
    'db_name' : args.database,
    'directory' : ''
    }
    config['alignment'] = {
    'evalue': args.evalue,
    'id_threshold': args.id_threshold,
    'pos_threshold': args.pos_threshold,
    'directory': '',
    'raw_output': 'aligned_peptides.xml',
    'filtered_output': 'aligned_peptides_filtered.tsv'
    }

    perform_alignment(config)

    # convert config dict to object configparser for the export
    config_obj = configparser.ConfigParser()
    config_obj.add_section("general")
    config_obj.add_section("digestion")
    config_obj.add_section("blastdb")
    config_obj.add_section("alignment")
    for section in config:
        for key in config[section]:
            config_obj.set(section, key, str(config[section][key]))

    configfile = os.path.join( args.outdir, 'config.ini' )
    with open(configfile, 'w') as configfile_fh:
        config_obj.write(configfile_fh)


if __name__ == '__main__':
    main()
    sys.exit(0)
