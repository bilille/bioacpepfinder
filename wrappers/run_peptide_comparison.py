#! /usr/bin/env python

import os,sys,inspect

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

import lib.peptide_lists_comparison 

lib.peptide_lists_comparison.main()

