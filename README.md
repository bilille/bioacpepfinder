# DESCRIPTION

BioacPepFinder.py is a tool to detect potentially bioactive peptides in proteins.

It performs the following steps:
- if the user gives protein sequences or identifiers as input: in silico enzymatic digestion of the input proteins with the program `rpg` [1].  
- alignment of the digested peptides against one or several databases of bioactive peptides.  
The alignment is made with the program `blast` [2].  
The bioactive peptides database must be given in fasta format or directly with the blastdb files.  
Currently we provide wrappers to convert tabular files from Biopep [3] and DRAMP [4] websites to fasta.  
- QSAR analysis for the detection of DPPIV or ACE activity [5] [6].  
- if the user gives a list of experimental peptides, the program only returns the results involving these peptides.  


# INSTALLATION

Please see [INSTALL.md](./INSTALL.md) file.


# USAGE

## Options

```bash
./BioacPepFinder.py -h
```

```usage: BioacPepFinder.py [options]

Pipeline for the detection of potentially bioactive peptides in proteins.

optional arguments:
  -h, --help            show this help message and exit
  -o [OUTDIR], --outdir [OUTDIR]
                        Path to your output directory (default: a folder is created in your input file directory)
  --force               If set, erase the content of --outdir if it already exists (/!\ be sure of what you are doing!)
  -i INPUT [INPUT ...], --input INPUT [INPUT ...]
                        Input file: proteic sequences (fasta) | list of genes/transcripts/proteic identifiers (txt) |
                        Peaks file (required)
  --input-type [INPUT_TYPE]
                        Type of input file: {'proteins', 'identifiers', 'Peaks'} (required)
  --id-type ID_TYPE [ID_TYPE ...]
                        Type of identifier (same number and order than --input-type entries): {'uniprot', 'ensembl',
                        'ncbi'} (default: uniprot)
  --mode [MODE]         Digestion mode. Either ‘s’, ‘sequential’, ‘c’ or ‘concurrent’ (default: s)
  -e ENZYMES [ENZYMES ...], --enzymes ENZYMES [ENZYMES ...]
                        Id of enzyme(s) to use (i.e. -e 0 5 10 to use enzymes 0, 5 and 10). Use rpg -l first to get enzyme
                        ids (default 42: trypsin)
  -m [MISSCLEAVAGE], --misscleavage [MISSCLEAVAGE]
                        Number of miss-cleavages allowed ([0-3] default 1)
  -d [DATABASE], --database [DATABASE]
                        Biopeptide database for usage with blastp (without extension)
  -f FASTA [FASTA ...], --fasta FASTA [FASTA ...]
                        One or several input fasta file containing biopeptide sequences
  --evalue [EVALUE]     Evalue threshold for blast (default 0.1)
  --id_threshold [ID_THRESHOLD]
                        Evalue threshold for percentage of identity (default 100)
  --pos_threshold [POS_THRESHOLD]
                        Evalue threshold for percentage of positives (default 0 - we do not filter on positives).
                        --pos_threshold N implies --id_threshold N.
  --exp [EXP]           A list of experimental peptides (one peptide per line). If given, only results involving one of
                        the given peptides will be printed.
```

## Output files

The output directory contains a subdirectory for each major step of the pipeline.


* input

The pipeline can accept several types of input:  

    - a list of gene / transcript / protein identifiers from Uniprot, Ensembl or NCBI  

    - a fasta file of protein sequences  

    - a tabulated file of peptides from Peaks [7]  


The content of `input` directory depends on the input type.  

    - identifiers:  
        - `proteins_sequences.fasta`: the raw, downloaded proteins  
        - `cleaned_proteins.fasta`: the proteins sequences cleaned from non-alphanumerical characters and duplicated entries  
    
    - protein sequences:  
        - `cleaned_proteins.fasta`: the input fasta file is cleaned from non-alphanumerical characters and duplicated entries  

    - peptides from Peaks:  
        - `peptides.fasta`: a fasta file with peptides sequences extracted from Peaks file  
        - `peptides_with_PTMs.tsv`: a tabulated file containing the informations about the PTMs (Post-Traductional Modification) present in input Peaks file  


* digestion (optional)

This subdirectory is created only if the input is either protein sequences or gene / transcript / protein identifiers.  

    - `digested_peptides_raw.tsv` contains raw digested peptides of in silico digestion program `rpg`, without misscleavages.  

    - `digested_peptides.fasta` and `digested_peptides.tsv` contains all digested peptides with at least 2 aminoacids
and with all combinations of maximum N misscleavages (N being given by the user).


* blastdb (optional)

This subdirectory is created only if a fasta file is given instead of blastdb files for the alignment.  

    - `complete_db.fasta`: contains all input bioactive peptides (concatenated if several databases have been given in input) and cleaned from non-alphanumerical characters and duplicated entries.  
 
    - `complete_db.phr`, `complete_db.pin`, `complete_db.pog`, `complete_db.psd`, `complete_db.psi`, `complete_db.psq`: blastdb files needed for the alignment.


* alignment

    - `aligned_peptides.xml`: contains raw output of alignment program `blast` in `xml` format.
The alignment is made between the digested peptides and the bioactive peptides databases.  

    - `aligned_peptides_filtered.tsv`: contains only results that meet the user-given criteria (identity and positives threshold), in `TSV` format.  


* QSAR

    - `QSAR_output.tsv`: contains scores of QSAR analysis for DPPIV and ACE activity for all digested peptides.  


* Other file(s)

    - `config.ini` contains all options used for the analysis.  
    - `log.txt` contains the output of `makeblastdb` command as well as information about things that went wrong.  
    - `final_output.tsv` summarises the results of alignment and QSAR.  
    - `final_filtered_peptides.tsv` (optional): if a list of experimental peptides is given by the user, this file contains only the results involving these peptides.  

The two TSV files are tabulated files with the following columns:  
peptide,
sequence,
targets_id,
targets_seq,
aln_lengths,
DPPIV_features_score,
DPPIV_zScore_IC50,
DPPIV_vScore_IC50,
ACE_zScore_IC50,
comment

If a peptide has a match against several bioactive peptides, all target identifiers are written in 'targets_id' column
separated with comma, as well as the target sequences and the alignement lengths.  

Please note that different bioactive peptides can have the same sequence.
If a peptide aligns against several bioactive peptides that *all* have the same sequence,
target sequence and alignment length will be reported only once.  


## Example

To test the pipeline please see the `example` folder.  
You can run the pipeline with `example/input_proteins.fasta` file as input proteins file
and `example/biopep.fasta` file to generate the database of bioactive peptides.

The `example/expected_output` folder contains an example of what you shall obtain.
Please see `example/expected_output/config.ini` to see the full list of parameters used for the execution.


# References

[1] Nicolas Maillet, Rapid Peptides Generator: fast and efficient in silico protein digestion, NAR Genomics and Bioinformatics, Volume 2, Issue 1, March 2020, lqz004, https://doi.org/10.1093/nargab/lqz004  
[2] Altschul SF, Gish W, Miller W, Myers EW, Lipman DJ. Basic local alignment search tool. J Mol Biol. 1990 Oct 5;215(3):403-10. doi: 10.1016/S0022-2836(05)80360-2. PMID: 2231712.  
[3] Minkiewicz P., Iwaniak A., Darewicz M., BIOPEP-UWM database of bioactive peptides: Current opportunities. International Journal of Molecular Sciences, 2019, 20, 5978, doi: 10.3390/ijms20235978.  
[4] Shi, G., Kang, X., Dong, F., Liu, Y., Zhu, N., Hu, Y., Xu, H., Lao, X., & Zheng, H. (2022). DRAMP 3.0: an enhanced comprehensive data repository of antimicrobial peptides. Nucleic acids research, 50(D1), D488–D496. https://doi.org/10.1093/nar/gkab651
[5] Nongonierma AB, FitzGerald RJ. Learnings from quantitative structure–activity relationship (QSAR) studies with respect to food protein-derived bioactive peptides: a review, RSC Adv. 2016, 6, 75400. 10.1039/c6ra12738j.  
[6] Nongonierma AB, FitzGerald RJ. Structure activity relationship modelling of milk protein-derived peptides with dipeptidyl peptidase IV (DPP-IV) inhibitory activity. Peptides. 2016 May;79:1-7. doi: 10.1016/j.peptides.2016.03.005. Epub 2016 Mar 15. PMID: 26988873.  
[7] https://www.bioinfor.com/peaks-studio/
