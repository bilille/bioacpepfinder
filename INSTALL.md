
# Install dependencies with conda

## Install conda

Please refer to [conda installation instructions](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)

conda usually comes with Python3.


## Create a conda environment

```
cd $PROJECT_DIR
conda env create -f bioacpepfinderEnv.yml
```

## If needed, configure shell

```
conda init bash
source ~/.bashrc
```

Depending on your shell, `bash` may be replaced with one of the following options:
  - fish
  - tcsh
  - xonsh
  - zsh
  - powershell


## Activate the environment

```
conda activate BioacPepFinderEnv
```


# Manual install

## Install Python3

```
sudo apt install python3
```


## Install python modules

```
pip3 install biopython
pip3 install requests
```


## Install rpg

```
pip3 install rpg
```


## Install blast+

```
sudo apt install ncbi-blast+
```

