


# User-defined enzyme Neutrase
ENZ = []

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

Y_0 = rule.Rule(0, "Y", True, 0) # Always cleaves before Y, except...
ENZ.append(Y_0)

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

V_0 = rule.Rule(0, "V", True, 0) # Always cleaves before V, except...
ENZ.append(V_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Neutrase", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1
