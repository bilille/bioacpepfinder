


# User-defined enzyme Flavopro_Umami
ENZ = []

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

K_0 = rule.Rule(0, "K", True, 0) # Always cleaves before K, except...
ENZ.append(K_0)

M_0 = rule.Rule(0, "M", True, 0) # Always cleaves before M, except...
ENZ.append(M_0)

E_0 = rule.Rule(0, "E", True, 0) # Always cleaves before E, except...
ENZ.append(E_0)

V_0 = rule.Rule(0, "V", True, 0) # Always cleaves before V, except...
ENZ.append(V_0)

T_0 = rule.Rule(0, "T", True, 0) # Always cleaves before T, except...
ENZ.append(T_0)

C_0 = rule.Rule(0, "C", True, 0) # Always cleaves before C, except...
ENZ.append(C_0)

Q_0 = rule.Rule(0, "Q", True, 0) # Always cleaves before Q, except...
ENZ.append(Q_0)

R_0 = rule.Rule(0, "R", True, 0) # Always cleaves before R, except...
ENZ.append(R_0)

S_0 = rule.Rule(0, "S", True, 0) # Always cleaves before S, except...
ENZ.append(S_0)

Y_0 = rule.Rule(0, "Y", True, 0) # Always cleaves before Y, except...
ENZ.append(Y_0)

A_0 = rule.Rule(0, "A", True, 0) # Always cleaves before A, except...
ENZ.append(A_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Flavopro_Umami", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1
