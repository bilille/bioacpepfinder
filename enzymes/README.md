# How to add custom enzymes to `rpg`

Custom enzymes cleaving rules are described in file `~/rpg_user.py`.  
If you want to delete your custom enzymes, simply delete this file. It will be created again at next `rpg` call.  
Currently there is no way of simply deleting one custom enzyme, unless you manually delete the corresponding lines in the file.


## Method 1: add manually with `rpg`

Follow [rpg instructions.](https://rapid-peptide-generator.readthedocs.io/en/latest/userguide.html#creating-a-new-enzyme)


## Method 2: add one or several of the enzymes we provide

Concatenate the corresponding files of the enzymes you want to add at the end of your file `~/rpg_user.py`.

```bash
cat enzyme1.py [enzyme2.py ...] >> ~/rpg_user.py
```


## Method 3: use the `rpg_user.py` file we provide

If you have no custom enzymes and you want to add at once all the custom enzymes we provide,
you can simply replace your file `~/rpg_user.py` by the provided file `rpg_user.py`.  

```bash
rm ~/rpg_user.py
cp $PATH_TO_THIS_FOLDER/rpg_user.py ~/
```

It contains the following enzymes in this order:  

* Pancreatin
* Neutrase
* Promod_24L
* Promod_184P_MDP
* Flavopro_Umami
* Promod_950L
* Promod_144GL


/!\ Be aware that if you already have some custom enzymes, they will be deleted.
 
