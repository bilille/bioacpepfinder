


# User-defined enzyme Promod_24L
ENZ = []

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

T_0 = rule.Rule(0, "T", True, 0) # Always cleaves before T, except...
ENZ.append(T_0)

C_0 = rule.Rule(0, "C", True, 0) # Always cleaves before C, except...
ENZ.append(C_0)

M_0 = rule.Rule(0, "M", True, 0) # Always cleaves before M, except...
ENZ.append(M_0)

G_0 = rule.Rule(0, "G", True, 0) # Always cleaves before G, except...
ENZ.append(G_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Promod_24L", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1
