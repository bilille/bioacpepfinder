


# User-defined enzyme Promod_950L
ENZ = []

Q_0 = rule.Rule(0, "Q", True, 0) # Always cleaves before Q, except...
ENZ.append(Q_0)

S_0 = rule.Rule(0, "S", True, 0) # Always cleaves before S, except...
ENZ.append(S_0)

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

Y_0 = rule.Rule(0, "Y", True, 0) # Always cleaves before Y, except...
ENZ.append(Y_0)

R_0 = rule.Rule(0, "R", True, 0) # Always cleaves before R, except...
ENZ.append(R_0)

V_0 = rule.Rule(0, "V", True, 0) # Always cleaves before V, except...
ENZ.append(V_0)

H_0 = rule.Rule(0, "H", True, 0) # Always cleaves before H, except...
ENZ.append(H_0)

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

K_0 = rule.Rule(0, "K", True, 0) # Always cleaves before K, except...
ENZ.append(K_0)

T_0 = rule.Rule(0, "T", True, 0) # Always cleaves before T, except...
ENZ.append(T_0)

M_0 = rule.Rule(0, "M", True, 0) # Always cleaves before M, except...
ENZ.append(M_0)

C_0 = rule.Rule(0, "C", True, 0) # Always cleaves before C, except...
ENZ.append(C_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Promod_950L", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1
