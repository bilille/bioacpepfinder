


# User-defined enzyme Pancreatin
ENZ = []

R_1 = rule.Rule(0, "R", True, 1) # Always cleaves after R, except...
ENZ.append(R_1)

K_1 = rule.Rule(0, "K", True, 1) # Always cleaves after K, except...
ENZ.append(K_1)

Y_1 = rule.Rule(0, "Y", True, 1) # Always cleaves after Y, except...
ENZ.append(Y_1)

W_1 = rule.Rule(0, "W", True, 1) # Always cleaves after W, except...
ENZ.append(W_1)

F_1 = rule.Rule(0, "F", True, 1) # Always cleaves after F, except...
ENZ.append(F_1)

L_1 = rule.Rule(0, "L", True, 1) # Always cleaves after L, except...
ENZ.append(L_1)

V_1 = rule.Rule(0, "V", True, 1) # Always cleaves after V, except...
ENZ.append(V_1)

I_1 = rule.Rule(0, "I", True, 1) # Always cleaves after I, except...
ENZ.append(I_1)

A_1 = rule.Rule(0, "A", True, 1) # Always cleaves after A, except...
ENZ.append(A_1)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Pancreatin", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1
