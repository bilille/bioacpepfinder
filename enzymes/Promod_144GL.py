


# User-defined enzyme Promod_144GL
ENZ = []

R_0 = rule.Rule(0, "R", True, 0) # Always cleaves before R, except...
ENZ.append(R_0)

Q_0 = rule.Rule(0, "Q", True, 0) # Always cleaves before Q, except...
ENZ.append(Q_0)

S_0 = rule.Rule(0, "S", True, 0) # Always cleaves before S, except...
ENZ.append(S_0)

T_0 = rule.Rule(0, "T", True, 0) # Always cleaves before T, except...
ENZ.append(T_0)

A_0 = rule.Rule(0, "A", True, 0) # Always cleaves before A, except...
ENZ.append(A_0)

K_0 = rule.Rule(0, "K", True, 0) # Always cleaves before K, except...
ENZ.append(K_0)

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Promod_144GL", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1
