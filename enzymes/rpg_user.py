from rpg import enzyme
from rpg import rule
from rpg import enzymes_definition

AVAILABLE_ENZYMES_USER = []
CPT_ENZ = enzymes_definition.CPT_ENZ

### ENZYMES DECLARATION ###



# User-defined enzyme Pancreatin
ENZ = []

R_1 = rule.Rule(0, "R", True, 1) # Always cleaves after R, except...
ENZ.append(R_1)

K_1 = rule.Rule(0, "K", True, 1) # Always cleaves after K, except...
ENZ.append(K_1)

Y_1 = rule.Rule(0, "Y", True, 1) # Always cleaves after Y, except...
ENZ.append(Y_1)

W_1 = rule.Rule(0, "W", True, 1) # Always cleaves after W, except...
ENZ.append(W_1)

F_1 = rule.Rule(0, "F", True, 1) # Always cleaves after F, except...
ENZ.append(F_1)

L_1 = rule.Rule(0, "L", True, 1) # Always cleaves after L, except...
ENZ.append(L_1)

V_1 = rule.Rule(0, "V", True, 1) # Always cleaves after V, except...
ENZ.append(V_1)

I_1 = rule.Rule(0, "I", True, 1) # Always cleaves after I, except...
ENZ.append(I_1)

A_1 = rule.Rule(0, "A", True, 1) # Always cleaves after A, except...
ENZ.append(A_1)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Pancreatin", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1



# User-defined enzyme Neutrase
ENZ = []

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

Y_0 = rule.Rule(0, "Y", True, 0) # Always cleaves before Y, except...
ENZ.append(Y_0)

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

V_0 = rule.Rule(0, "V", True, 0) # Always cleaves before V, except...
ENZ.append(V_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Neutrase", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1



# User-defined enzyme Promod_24L
ENZ = []

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

T_0 = rule.Rule(0, "T", True, 0) # Always cleaves before T, except...
ENZ.append(T_0)

C_0 = rule.Rule(0, "C", True, 0) # Always cleaves before C, except...
ENZ.append(C_0)

M_0 = rule.Rule(0, "M", True, 0) # Always cleaves before M, except...
ENZ.append(M_0)

G_0 = rule.Rule(0, "G", True, 0) # Always cleaves before G, except...
ENZ.append(G_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Promod_24L", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1



# User-defined enzyme Promod_184P_MDP
ENZ = []

R_0 = rule.Rule(0, "R", True, 0) # Always cleaves before R, except...
ENZ.append(R_0)

K_0 = rule.Rule(0, "K", True, 0) # Always cleaves before K, except...
ENZ.append(K_0)

A_0 = rule.Rule(0, "A", True, 0) # Always cleaves before A, except...
ENZ.append(A_0)

Y_0 = rule.Rule(0, "Y", True, 0) # Always cleaves before Y, except...
ENZ.append(Y_0)

G_0 = rule.Rule(0, "G", True, 0) # Always cleaves before G, except...
ENZ.append(G_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Promod_184P_MDP", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1



# User-defined enzyme Flavopro_Umami
ENZ = []

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

K_0 = rule.Rule(0, "K", True, 0) # Always cleaves before K, except...
ENZ.append(K_0)

M_0 = rule.Rule(0, "M", True, 0) # Always cleaves before M, except...
ENZ.append(M_0)

E_0 = rule.Rule(0, "E", True, 0) # Always cleaves before E, except...
ENZ.append(E_0)

V_0 = rule.Rule(0, "V", True, 0) # Always cleaves before V, except...
ENZ.append(V_0)

T_0 = rule.Rule(0, "T", True, 0) # Always cleaves before T, except...
ENZ.append(T_0)

C_0 = rule.Rule(0, "C", True, 0) # Always cleaves before C, except...
ENZ.append(C_0)

Q_0 = rule.Rule(0, "Q", True, 0) # Always cleaves before Q, except...
ENZ.append(Q_0)

R_0 = rule.Rule(0, "R", True, 0) # Always cleaves before R, except...
ENZ.append(R_0)

S_0 = rule.Rule(0, "S", True, 0) # Always cleaves before S, except...
ENZ.append(S_0)

Y_0 = rule.Rule(0, "Y", True, 0) # Always cleaves before Y, except...
ENZ.append(Y_0)

A_0 = rule.Rule(0, "A", True, 0) # Always cleaves before A, except...
ENZ.append(A_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Flavopro_Umami", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1



# User-defined enzyme Promod_950L
ENZ = []

Q_0 = rule.Rule(0, "Q", True, 0) # Always cleaves before Q, except...
ENZ.append(Q_0)

S_0 = rule.Rule(0, "S", True, 0) # Always cleaves before S, except...
ENZ.append(S_0)

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

Y_0 = rule.Rule(0, "Y", True, 0) # Always cleaves before Y, except...
ENZ.append(Y_0)

R_0 = rule.Rule(0, "R", True, 0) # Always cleaves before R, except...
ENZ.append(R_0)

V_0 = rule.Rule(0, "V", True, 0) # Always cleaves before V, except...
ENZ.append(V_0)

H_0 = rule.Rule(0, "H", True, 0) # Always cleaves before H, except...
ENZ.append(H_0)

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

K_0 = rule.Rule(0, "K", True, 0) # Always cleaves before K, except...
ENZ.append(K_0)

T_0 = rule.Rule(0, "T", True, 0) # Always cleaves before T, except...
ENZ.append(T_0)

M_0 = rule.Rule(0, "M", True, 0) # Always cleaves before M, except...
ENZ.append(M_0)

C_0 = rule.Rule(0, "C", True, 0) # Always cleaves before C, except...
ENZ.append(C_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Promod_950L", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1



# User-defined enzyme Promod_144GL
ENZ = []

R_0 = rule.Rule(0, "R", True, 0) # Always cleaves before R, except...
ENZ.append(R_0)

Q_0 = rule.Rule(0, "Q", True, 0) # Always cleaves before Q, except...
ENZ.append(Q_0)

S_0 = rule.Rule(0, "S", True, 0) # Always cleaves before S, except...
ENZ.append(S_0)

T_0 = rule.Rule(0, "T", True, 0) # Always cleaves before T, except...
ENZ.append(T_0)

A_0 = rule.Rule(0, "A", True, 0) # Always cleaves before A, except...
ENZ.append(A_0)

K_0 = rule.Rule(0, "K", True, 0) # Always cleaves before K, except...
ENZ.append(K_0)

F_0 = rule.Rule(0, "F", True, 0) # Always cleaves before F, except...
ENZ.append(F_0)

L_0 = rule.Rule(0, "L", True, 0) # Always cleaves before L, except...
ENZ.append(L_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Promod_144GL", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1
