


# User-defined enzyme Promod_184P_MDP
ENZ = []

R_0 = rule.Rule(0, "R", True, 0) # Always cleaves before R, except...
ENZ.append(R_0)

K_0 = rule.Rule(0, "K", True, 0) # Always cleaves before K, except...
ENZ.append(K_0)

A_0 = rule.Rule(0, "A", True, 0) # Always cleaves before A, except...
ENZ.append(A_0)

Y_0 = rule.Rule(0, "Y", True, 0) # Always cleaves before Y, except...
ENZ.append(Y_0)

G_0 = rule.Rule(0, "G", True, 0) # Always cleaves before G, except...
ENZ.append(G_0)

ENZYME = enzyme.Enzyme(CPT_ENZ, "Promod_184P_MDP", ENZ, 0)
# Add it to available enzymes
AVAILABLE_ENZYMES_USER.append(ENZYME)
CPT_ENZ += 1
